﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication6.Models
{
    public class IndexModel
    {
        
        public string URL { get; set; }
        public string URLinLocal { get; set; }
        public List<string> URLloc = new List<string>();
        public List<URLXml> ux = new List<URLXml>();
        public double TotalTime { get; set; }
        public List<string> JsonParametr = new List<string>();

    }

    public class URLXml
    {
        public string node { get; set; }
        public int i { get; set; }
        public double timeexe { get; set; }

        public URLXml(string node, int i, double timeexe)
        {
            this.node = node;
            this.i = i;
            this.timeexe = timeexe;
        }
        
    }


    /*
    public class Dio
    {
        public int Id { get; set; }
        public double TimeUrl { get; set; }
        public Dictionary<int, double> diod = new Dictionary<int, double>();
    }

    public class time
    {
       
        public double timeexe { get; set; }
    }*/

}