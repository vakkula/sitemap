﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Xml;
using WebApplication6.Models;

namespace WebApplication6.Controllers
{
    public class CompController : Controller
    {
         double ti = 0;
        // GET: Comp
        public ActionResult Index(IndexModel model)
        {
            string path = model.URLinLocal; 
            XmlDocument XmlDoc = new XmlDocument();
            XmlDoc.Load(path);
            XmlElement root = XmlDoc.DocumentElement;
            int count = root.ChildNodes.Count;
            int i = 1;
            XmlNodeList nodesList = root.SelectNodes("*");
            string JsonStr = "";
            string str = "";

            using (DB db = new DB())
            {
                foreach (XmlNode n in nodesList)
                {
                    if (i == count + 1)
                    {
                        break;
                    }

                    string node = n.FirstChild.InnerText;

                    Task task = new Task(() => startDownload(node, ref ti));
                    task.Start();
                    task.Wait();
                    History history = new History { Id = i, URLDb = node, timeDb = ti };
                    model.ux.Add(new URLXml(node, i, ti));
                    db.Historys.Add(history);
                    db.SaveChanges();
                    model.TotalTime = model.TotalTime + ti;


                    JsonStr = "{\"ID\": \"" + i.ToString() + "\",\"time\": " + Math.Round(ti) + ",\"color\": \"#FF0F00\"},";
                    str += JsonStr;
                    model.JsonParametr.Add(JsonStr);
                    i++;
                }
            }
            ViewBag.Message2 = str;
            ViewBag.Message = model.TotalTime;
            //ViewBag.Message2 = GetItem(model);
            return View(model);
            }

       

        public void startDownload(string url, ref double ti)
        {
            WebClient web = new WebClient();
            Stopwatch sWatch = new Stopwatch();

            
            sWatch.Start();
            try
            {
                WebRequest request = WebRequest.Create(url);
               // web.DownloadFile(url, @"D:\\file.tmp"); //pars item
                WebResponse response = request.GetResponse();
                sWatch.Stop();

                double timeexe = sWatch.Elapsed.TotalMilliseconds;
                ti = timeexe;
                sWatch.Reset();
            }
            catch (Exception e)
            {
                ti = 0;
            }
            
        }
        
        /*public JsonResult GetItem(IndexModel mod)
        {
            Dio d = new Dio();
            var tim = mod.ux;
            foreach (var s in tim)
            {
                d.Id = s.i;
                d.TimeUrl = s.timeexe;
                d.diod.Add(d.Id,d.TimeUrl);
            }
            return Json(d.diod);
        }
        */
    }
}