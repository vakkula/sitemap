﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace WebApplication6.Controllers
{
    public class HomeController : Controller
    {


        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(Models.IndexModel model)
        {
            var mass = model.URL;
            var res = mass.Split('/');
            string FileSitemap = res.LastOrDefault();
            int found = mass.IndexOf(FileSitemap);
            string UrlSitemap = mass.Remove(found, FileSitemap.Length);
            using (WebClient myWebClient = new WebClient())
            {

                try
                {
                    string myStringWebResource = UrlSitemap + FileSitemap;
                    Uri uri = new Uri(myStringWebResource);
                    myWebClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(myWebClient_DownloadProgressChanged);
                    myWebClient.DownloadFile(uri, model.URLinLocal = @"D:\\" + FileSitemap);
                    //  myWebClient.DownloadFileAsync(myStringWebResource, model.URLinLocal = @"D:\\" + FileSitemap);
                    if (model.URLinLocal == null)
                    {
                        return RedirectToAction("Error", "Comp");

                    }

                    model.URLloc.Add(model.URLinLocal);

                }
                catch (Exception e)
                {
                    return RedirectToAction("Error", "Comp");
                }

            }

            return RedirectToAction("Index", "Comp", model);
        }
       
                public void myWebClient_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
                {
                   
                   ViewBag.progress =  e.ProgressPercentage;
                }
                


        /*
        public ActionResult myWebClient_DPC(object sender, DownloadProgressChangedEventArgs e)
        {
            ViewBag.progress = e.ProgressPercentage;
            return PartialView(ViewBag.progress);
        }

        */

    }
}